# This dockerfile utilizes components licensed by their respective owners/authors.

FROM microsoft/dotnet:2.0.0-runtime-nanoserver-1709

LABEL Description="IIS" Vendor="Microsoft" Version="10"

CMD [ "ping", "localhost", "-t" ]